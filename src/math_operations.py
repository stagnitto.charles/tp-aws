def division(num, denum) -> int:
    if denum == 0:
        raise ValueError("Division by zero is undefined.")
    return num // denum